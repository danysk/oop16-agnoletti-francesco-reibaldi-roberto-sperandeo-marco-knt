package knight;

/**
 * contiene tutti gli id delle entità presenti nel gioco
 */
public enum Id {
	player, firstGhost, secondGhost, thirdGhost, firstBoss, secondBoss, wall, ground, powerUp, energyWave, energyWaveBoss, crystal, specialCystal, door, crown;																																				// boss
}
